#!/bin/bash

set -e

rm -rf results
mkdir results

wget https://raw.githubusercontent.com/itsiprikshit/uFS/develop/cfs_bench/exprs/artifact_eval/artifact_eval.sh
bash artifact_eval.sh init cloudlab

# Need to restart here, but order sage automatically restarts after initialization
# echo "ae init-after-reboot cloudlab >> aeboot.txt" >> ~/.bashrc

# echo "@reboot ae init-after-reboot cloudlab >> ~/rebootcmd.txt" >> /var/spool/cron/crontabs/prikshit
# echo "@reboot ae init-after-reboot cloudlab >> ~/rebootcmd.txt" >> /etc/cron.d/aeboot

# sudo cp ./aeboot.service /etc/systemd/system/aeboot.service
# sudo systemctl daemon-reload
# sudo systemctl enable aeboot.service