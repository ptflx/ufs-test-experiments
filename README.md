# ufs-test-experiments

This repository contains the instructions for running the **uFS Paper Artifact Reproduction** case study from the paper by Duplyain et al. at ATC '23.

First and foremost, before running the ordersage for this case study, replace the _config.py_ of ordersage with the following configuration. It is same as the configuration in _config.py_ in this repo.

```
"""
SSH Configurations
"""
# pre-allocated worker nodes must be added here by hostname
workers = ["worker"]
user = "ordersage"
keyfile = "/local/id_rsa"
port_num = 22

"""
Experiemnt Repo
"""
repo = "https://gitlab.flux.utah.edu/ptflx/ufs-test-experiments.git"

"""
Filepaths and commands on worker node
"""
init_script_call = "cd ufs-test-experiments && bash initialize.sh"
exp_script_call = "cd ufs-test-experiments && python3 exp_config.py"
results_dir = "~/ufs-test-experiments/results"
results_file = "results.txt"

"""
Controller options
"""
# specifies the number of runs for both fixed and random order
n_runs = 10
# specifies if random and fixed runs should be interleaved or not
interleave = True
# prints STDOUT of workers to console
verbose = True
# Ignore reset command for debugging purposes
reset = True
# Set your own random seed
seed = None

"""
Instrumentation options, in the order they need to be added to the experiment
"""
instrumentation_modules = ["perf"]
```

This case study evaluates [uFS](https://github.com/WiscADSL/uFS) (uFS is a filesystem semi-microkernel that designs for device performance delivery and scalability) and runs a set of benchmark jobs to identify any ordering effects.

We forked the original uFS repo at [this](https://github.com/WiscADSL/uFS/tree/25e50387e2f7a3170931bb9b0f60c4fe8a1ecf61) commit and made some adjustments to the setup scripts so that the benchmarking can be done with ordersage. All the changes are in the [develop](https://github.com/itsiprikshit/uFS/tree/develop) branch. Our evaluation is based on this version of the uFS codebase.

The forked repo can be found [here](https://github.com/itsiprikshit/uFS/tree/develop) and the git diff of the changes we made is [here](https://github.com/WiscADSL/uFS/compare/main...itsiprikshit:uFS:develop). As you can see we did not make any changes to the actual uFS source code. The changes were simply made to the artifact evaluation scripts provided the authors of uFS.

uFS artifact evaluation instructions are [here](https://github.com/itsiprikshit/uFS/tree/main/cfs_bench/exprs/artifact_eval) and we strictly followed them. We selected the Microbenchmarks with single-threaded uFS and ext4 (both without journaling.) Their scripts run all 32 workloads in sequence; we modified them to be able to run one workload at a time as individual tests.
