"""
SSH Configurations
"""
# pre-allocated worker nodes must be added here by hostname
workers = ["worker"]
user = "ordersage"
keyfile = "/local/id_rsa"
port_num = 22

"""
Experiemnt Repo
"""
repo = "https://gitlab.flux.utah.edu/ptflx/ufs-test-experiments.git"

"""
Filepaths and commands on worker node
"""
init_script_call = "cd ufs-test-experiments && bash initialize.sh"
exp_script_call = "cd ufs-test-experiments && python3 exp_config.py"
results_dir = "~/ufs-test-experiments/results"
results_file = "results.txt"

"""
Controller options
"""
# specifies the number of runs for both fixed and random order
n_runs = 10
# specifies if random and fixed runs should be interleaved or not
interleave = True
# prints STDOUT of workers to console
verbose = True
# Ignore reset command for debugging purposes
reset = True
# Set your own random seed
seed = None

"""
Instrumentation options, in the order they need to be added to the experiment
"""
instrumentation_modules = ["perf"]