#!/bin/bash

job=ADPS
fs="ext4nj"
benchmark_result_dest="results/results.txt"

ae init-after-reboot cloudlab

ae run microbench $fs --numapp=1 --jobs=$job

ae plot microbench single $fs nocmp --numapp=1 --jobs=$job

now=$(date +"%Y:%m:%d_%H:%m:%S")
raw_dest="results/$job"_"$fs"_"$now.csv"

cp ./AE_DATA/DATA_microbench_$fs/$job.csv $raw_dest

run_status=$?

lno=1
while IFS=',' read -r l1 l2 l3 l4
do
    if [ $lno -ge 2 ]; then
        echo "$l3" >> $benchmark_result_dest
    fi

    lno=$((lno+1))
done < $raw_dest

exit $run_status